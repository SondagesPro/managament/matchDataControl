<?php

/**
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @copyright 2020-2024 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license GPL
 * @version 1.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class matchDataControl extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Match data by specific value.';
    protected static $name = 'matchDataControl';

    /** inheritdoc */
    public $allowedPublicMethods = [
        'actionSettings',
        'actionSaveSettings',
    ];

    public function init()
    {
        /* Add settings update */
        $this->subscribe('beforeToolsMenuRender');
        /* Set the value */
        $this->subscribe('afterSurveyComplete');
        /* Do match data when load */
        $this->subscribe('getPluginTwigPath', 'matchDataByTwig');
        /* Update question part */
        $this->subscribe('beforeQuestionRender');
        /* Dowload file */
        $this->subscribe('newDirectRequest');
    }


    /**
     * see beforeToolsMenuRender event
     * @deprecated ? See https://bugs.limesurvey.org/view.php?id=15476
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aMenuItem = array(
            'label' => $this->translate('Data match'),
            'iconClass' => 'fa fa-link',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $event->append('menuItems', array($menuItem));
    }

    public function getPluginTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
        $this->unsubscribe('getPluginTwigPath');
    }

    public function matchDataByTwig()
    {
        $this->unsubscribe('getPluginTwigPath');
        $surveyId = Yii::app()->getRequest()->getQuery('sid', Yii::app()->getRequest()->getParam('surveyid'));
        if (!$surveyId) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            return;
        }
        $surveyId = intval($surveyId);
        if (!$this->get('matchQuestion1', 'Survey', $surveyId, null)) {
            return;
        }
        if (!$this->get('matchQuestion2', 'Survey', $surveyId, null)) {
            return;
        }
        if (!$this->get('matchedQuestions', 'Survey', $surveyId, null)) {
            return;
        }
        $this->matchData($surveyId);
    }

    /**
     * The main function
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $unavailableQuestion = array();
        $matchQuestion1 = Yii::app()->getRequest()->getPost('matchQuestion1');
        $this->set('matchQuestion1', $matchQuestion1, 'Survey', $surveyId);
        if ($matchQuestion1) {
            $unavailableQuestion[] = Yii::app()->getRequest()->getPost('matchQuestion1');
        }
        $matchQuestion2 = Yii::app()->getRequest()->getPost('matchQuestion2');
        if (!in_array($matchQuestion2, $unavailableQuestion)) {
            $this->set('matchQuestion2', $matchQuestion2, 'Survey', $surveyId);
            if ($matchQuestion2) {
                $unavailableQuestion[] = $matchQuestion2;
            }
        } else {
            if ($matchQuestion2) {
                Yii::app()->setFlashMessage($this->translate('Matched question must be different.'), 'error');
            }
            $this->set('matchQuestion2', null, 'Survey', $surveyId);
        }
        $otherFilters = Yii::app()->getRequest()->getPost('otherFilters');
        $this->set('otherFilters', $otherFilters, 'Survey', $surveyId);
        $otherFilters = $this->getOtherFilters($surveyId);

        $matchQuestionData = Yii::app()->getRequest()->getPost('matchQuestionData');
        if (!in_array($matchQuestionData, $unavailableQuestion)) {
            $this->set('matchQuestionData', $matchQuestionData, 'Survey', $surveyId);
            if ($matchQuestionData) {
                $unavailableQuestion[] = $matchQuestionData;
            }
        } else {
            Yii::app()->setFlashMessage($this->translate('Question for matching data can not be in match question control.'), 'error');
            $this->set('matchQuestionData', null, 'Survey', $surveyId);
        }
        $matchQuestionCount = Yii::app()->getRequest()->getPost('matchQuestionCount');
        if (!in_array($matchQuestionCount, $unavailableQuestion)) {
            $this->set('matchQuestionCount', $matchQuestionCount, 'Survey', $surveyId);
            if ($matchQuestionCount) {
                $unavailableQuestion[] = $matchQuestionCount;
            }
        } else {
            Yii::app()->setFlashMessage($this->translate('Question for count difference can not be in match question control.'), 'error');
            $this->set('matchQuestionCount', null, 'Survey', $surveyId);
        }
        $matchQuestionState = Yii::app()->getRequest()->getPost('matchQuestionState');
        if (!in_array($matchQuestionState, $unavailableQuestion)) {
            $this->set('matchQuestionState', $matchQuestionState, 'Survey', $surveyId);
            if ($matchQuestionState) {
                $unavailableQuestion[] = $matchQuestionState;
            }
        } else {
            Yii::app()->setFlashMessage($this->translate('Question for state can not be in match question control.'), 'error');
            $this->set('matchQuestionState', null, 'Survey', $surveyId);
        }

        $matchedQuestions = (array) Yii::app()->getRequest()->getPost('matchedQuestions');
        $matchedQuestions = array_filter($matchedQuestions, function ($questionCode) use ($unavailableQuestion) {
            return !in_array($questionCode, $unavailableQuestion);
        });
        $this->set('matchedQuestions', $matchedQuestions, 'Survey', $surveyId);

        /* Basic setting */
        $exportNoMatched = Yii::app()->getRequest()->getPost('exportNoMatched');
        $this->set('exportNoMatched', $exportNoMatched, 'Survey', $surveyId);
        $exportCount = Yii::app()->getRequest()->getPost('exportCount');
        $this->set('exportCount', $exportCount, 'Survey', $surveyId);
        $exportState = Yii::app()->getRequest()->getPost('exportState');
        $this->set('exportState', $exportState, 'Survey', $surveyId);
        $exportState = Yii::app()->getRequest()->getPost('useexpression');
        $this->set('useexpression', $exportState, 'Survey', $surveyId);

        /* Multilingual */
        $aLanguages = Survey::model()->findByPk($surveyId)->getAllLanguages();
        $aCurrentFilename = array();
        foreach ($aLanguages as $language) {
            $aCurrentFilename[$language] = Yii::app()->getRequest()->getPost('filename_' . $language, '');
        }
        $this->set('filename', $aCurrentFilename, 'Survey', $surveyId);
        $aCurrentNomatchstring = array();
        foreach ($aLanguages as $language) {
            $aCurrentFilename[$language] = Yii::app()->getRequest()->getPost('nomatchstring_' . $language, '');
        }
        $this->set('nomatchstring', $aCurrentFilename, 'Survey', $surveyId);

        if (Yii::app()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('surveyAdministration/view', array('surveyid' => $surveyId));
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }
    /**
     * The main function
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $language = $oSurvey->language;
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $surveyId = intval($surveyId);
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['lang'] = array(
            'Data to be matched' => gT("Data to be matched"),
        );
        $aSettings = array();

        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, Yii::app()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        /* Other filter questions */
        $aFiltersQuestions = $surveyColumnsInformation->allQuestionListData();
        /* 2 question to match */
        /* Available questions for match */
        $surveyColumnsInformation->restrictToType = ['L','O','!','*','S','T','U'];
        $aMatchesQuestions = $surveyColumnsInformation->allQuestionListData();

        $aMatchQuestionSettings = array(
            'matchQuestion1' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                    'options' => $aMatchesQuestions['options'],
                ),
                'label' => $this->translate("Question for matching"),
                'options' => $aMatchesQuestions['data'],
                'current' => $this->get('matchQuestion1', 'Survey', $surveyId, null),
                'help' => "",
            ),
            'matchQuestion2' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                    'options' => $aMatchesQuestions['options'],
                ),
                'label' => $this->translate("Related question for matching"),
                'options' => $aMatchesQuestions['data'],
                'current' => $this->get('matchQuestion2', 'Survey', $surveyId, null),
                'help' => "",
            ),
            'otherFilters' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'placeholder' => $this->translate("None"),
                    'unselectValue' => "",
                    'multiple' => true,
                    'options' => $aFiltersQuestions['options'],
                ),
                'selectOptions' => array(
                    'placeholder' => $this->translate("None"),
                ),
                'label' => $this->translate("Other filter"),
                'options' => $aFiltersQuestions['data'],
                'current' => $this->get('otherFilters', 'Survey', $surveyId, null),
                'help' => "",
            ),
        );

        $aSettings[$this->translate("Questions for matching")] = $aMatchQuestionSettings;

        /* Question for resume */
        $surveyColumnsInformation->restrictToType = ['T','U'];
        $aResumeQuestions = $surveyColumnsInformation->allQuestionListData();
        /* Question for count */
        $surveyColumnsInformation->restrictToType = ['S','N'];
        $aCountQuestions = $surveyColumnsInformation->allQuestionListData();
        /* Question for state */
        $surveyColumnsInformation->restrictToType = ['S'];
        $aStateQuestions = $surveyColumnsInformation->allQuestionListData();


        $aMatchQuestionSettings = array(
            'matchQuestionData' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                    'options' => $aResumeQuestions['options'],
                ),
                'label' => $this->translate("Question for matching data"),
                'options' => $aResumeQuestions['data'],
                'current' => $this->get('matchQuestionData', 'Survey', $surveyId, null),
                'help' => $this->translate("Data are saved as a json data with : related reponse id, state of related response and data. This question show a table with difference."),
            ),
            'matchQuestionCount' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                    'options' => $aCountQuestions['options'],
                ),
                'label' => $this->translate("Question for count of difference"),
                'options' => $aCountQuestions['data'],
                'current' => $this->get('matchQuestionCount', 'Survey', $surveyId, null),
                'help' => "",
            ),
            'matchQuestionState' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'empty' => $this->translate("None"),
                    'options' => $aStateQuestions['options'],
                ),
                'label' => $this->translate("Question for state"),
                'options' => $aStateQuestions['data'],
                'current' => $this->get('matchQuestionState', 'Survey', $surveyId, null),
                'help' => sprintf($this->translate("Empty if no compare data, %s for not submitted data and %s for submitted data"), 'draft', 'submitted'),
            ),
        );
        $aSettings[$this->translate("Questions for data and resume")] = $aMatchQuestionSettings;

        /* Question to be matched */
        $surveyColumnsInformation->restrictToType = ['L','O','!','N','*','S','T','U'];
        $aMatchesQuestions = $surveyColumnsInformation->allQuestionListData();

        $aMatchQuestions = array(
            'matchedQuestions' => array(
                'type' => 'select',
                'htmlOptions' => array(
                    'multiple' => true,
                    'placeholder' => $this->translate("None"),
                    'unselectValue' => "",
                    'options' => $aMatchesQuestions['options'],
                ),
                'selectOptions' => array(
                    'placeholder' => $this->translate("None"),
                ),
                'label' => $this->translate("Question to be matched"),
                'options' => $aMatchesQuestions['data'],
                'current' => $this->get('matchedQuestions', 'Survey', $surveyId, null),
                'help' => "",
            ),
        );
        $aSettings[$this->translate("Questions to be matched")] = $aMatchQuestions;

        /* Dowload links */
        /* Move it to view ? */
        $htmlLink  = "<div class='well'>";
        $htmlLink .= "<div class='h4'>" . $this->translate("Links for download differences file") . "</div>";
        $htmlLink .= "<ul>";
        $htmlLink .= "<li>" . Chtml::link(
            $this->translate("All data"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId)
        ) . '</li>';
        $htmlLink .= "<li>" . Chtml::link(
            $this->translate("Only completed data (related)"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId, 'state' => 'completed')
        ) . '</li>';
        $htmlLink .= "<li>" . Chtml::link(
            $this->translate("Only if there are a difference between matching data"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId, 'matchdiff' => '1')
        ) . '</li>';
        $htmlLink .= "<li>" . Chtml::link(
            $this->translate("Only differences"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId, 'diff' => '1')
        ) . '</li>';
        $htmlLink .= "</ul>";
        $htmlLink .= "</div>";
        $htmlLink .= "<div class='well'>";
        $htmlLink .= "<div class='h4'>" . $this->translate("URL to download differences file inside survey") . "</div>";
        $htmlLink .= "<ul>";
        $htmlLink .= "<li>"
                  . CHtml::tag('strong', array(), $this->translate("All data with token"))
                  . Chtml::tag(
                      "code",
                      array(),
                      str_replace(
                          array('SID','TOKEN'),
                          array('{SID}','{TOKEN}'),
                          Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId,'token' => 'TOKEN'))
                      )
                  )
                  . "</li>";
        $htmlLink .= "<li>"
                  . CHtml::tag('strong', array(), $this->translate("All data related to current response"))
                  . Chtml::tag(
                      "code",
                      array(),
                      str_replace(
                          array('SID','SAVEDID'),
                          array('{SID}','{SAVEDID}'),
                          Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId,'srid' => 'SAVEDID'))
                      )
                  )
                  . "</li>";
        $htmlLink .= "<li>"
                  . CHtml::tag('strong', array(), $this->translate("All data with token with only completed related data"))
                  . Chtml::tag(
                      "code",
                      array(),
                      str_replace(
                          array('SID','TOKEN'),
                          array('{SID}','{TOKEN}'),
                          Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId,'token' => 'TOKEN','state' => 'completed'))
                      )
                  )
                  . "</li>";
        $htmlLink .= "<li>"
                  . CHtml::tag('strong', array(), $this->translate("All data with difference with related matched data (export all data where even if there are only one diference)."))
                  . Chtml::tag(
                      "code",
                      array(),
                      str_replace(
                          array('SID','TOKEN'),
                          array('{SID}','{TOKEN}'),
                          Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId,'token' => 'TOKEN','matchdiff' => '1'))
                      )
                  )
                  . "</li>";
        $htmlLink .= "<li>"
                  . CHtml::tag('strong', array(), $this->translate("Only differences"))
                  . Chtml::tag(
                      "code",
                      array(),
                      str_replace(
                          array('SID','TOKEN'),
                          array('{SID}','{TOKEN}'),
                          Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid' => $surveyId,'token' => 'TOKEN','diff' => '1'))
                      )
                  )
                  . "</li>";
        $htmlLink .= "</ul>";
        $htmlLink .= "</div>";
        $aExportLinks = array(
            'exportNoMatched' => array(
                'type' => 'select',
                //~ 'htmlOptions'=>array(
                    //~ 'empty'=>$this->translate("Inherit (leave default)"),
                //~ ),
                'options' => array(
                    '1' => gT("Yes"),
                    '0' => gT("No"),
                ),
                'label' => $this->translate("Export data without match."),
                'help' => $this->translate("Export on e line whith data value ith information about no matched data"),
                'current' => $this->get('exportNoMatched', 'Survey', $surveyId, 1),
            ),
            'matchedQuestionLink' => array(
                'type' => 'info',
                'content' => $htmlLink
            ),
            'exportCount' => array(
                'type' => 'select',
                //~ 'htmlOptions'=>array(
                    //~ 'empty'=>$this->translate("Inherit (leave default)"),
                //~ ),
                'options' => array(
                    '1' => gT("Yes"),
                    '0' => gT("No"),
                ),
                'label' => $this->translate("Export count column"),
                'help' => $this->translate("Only if count column exist"),
                'current' => $this->get('exportCount', 'Survey', $surveyId, 1),
            ),
            'exportState' => array(
                'type' => 'select',
                //~ 'htmlOptions'=>array(
                    //~ 'empty'=>$this->translate("Inherit (leave default)"),
                //~ ),
                'options' => array(
                    '1' => gT("Yes"),
                    '0' => gT("No"),
                ),
                'label' => $this->translate("Export state column"),
                'help' => $this->translate("Only if state column exist"),
                'current' => $this->get('exportState', 'Survey', $surveyId, 1),
            ),
        );
        $aSettings[$this->translate("Export links")] = $aExportLinks;

        /* Text string, by language .... need to be adapted if i remind ... */
        $aLanguages = Survey::model()->findByPk($surveyId)->getAllLanguages();
        $aStringSetting = array();
        $aCurrentFilename = $this->get('filename', 'Survey', $surveyId);
        foreach ($aLanguages as $language) {
            $aStringSetting['filename_' . $language] = array(
                'name' => 'filename',
                'type' => 'string',
                'label' => sprintf($this->translate("Name of the file (%s)"), $language),
                'help' => $this->translate("You can use Expression manager, using state. But this don't load any data of related survey. The final filename was filtered for security."),
                'current' => !empty($aCurrentFilename[$language]) ? $aCurrentFilename[$language] : '',
            );
        }
        $aCurrentNomatchstring = $this->get('nomatchstring', 'Survey', $surveyId);
        foreach ($aLanguages as $language) {
            $aStringSetting['nomatchstring_' . $language] = array(
                'name' => 'nomatchstring',
                'type' => 'string',
                'label' => sprintf($this->translate("String for no match data (%s)"), $language),
                'htmlOptions' => array(
                    'placeholder' => $this->translate("No match data.", 'unescaped', $language),
                ),
                'current' => !empty($aCurrentNomatchstring[$language]) ? $aCurrentNomatchstring[$language] : '',
            );
        }
        $aStringSetting['useexpression'] = array(
            'type' => 'select',
            //~ 'htmlOptions'=>array(
                //~ 'empty'=>$this->translate("Inherit (leave default)"),
            //~ ),
            'options' => array(
                '0' => gT("No"),
                'token' => $this->translate("With token"),
                'all' => gT("Yes"),
            ),
            'label' => $this->translate("Use expression manager on string."),
            'help' => $this->translate("This use current expression state with current survey (can be different that export survey)"),
            'current' => $this->get('useexpression', 'Survey', $surveyId, 0),
        );

        $aSettings[$this->translate("Language settings")] = $aStringSetting;

        $aData['aSettings'] = $aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveSettings','surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('surveyAdministration/view', array('surveyid' => $surveyId)),
        );
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }

    /**
     * Translation replacer
     * @param string $string to b translated
     * @param string $sEscapeMode default to unescaped
     * @param string $sLanguage for translation , default to current Yii lang
     * @return string translated string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return $this->gT($string, $sEscapeMode, $sLanguage);
    }

    /**
     * Match the data in current $_SESSION
     * @param @surveyId
     * @return void
     */
    private function matchData($surveyId)
    {
        if (empty($_SESSION['survey_' . $surveyId]['srid'])) {
            return;
        }
        $srid = $_SESSION['survey_' . $surveyId]['srid'];
        $relatedData = $this->findRelatedDataSession($surveyId, $srid);
        if (empty($relatedData)) {
            return;
        }
        $this->fillExtraQuestions($surveyId, $srid);
    }

    public function afterSurveyComplete()
    {
        /* Save the data in the other related */
        $surveyId = $this->getEvent()->get('surveyId');
        $responseId = $this->getEvent()->get('responseId');
        if (!$surveyId || !$responseId) {
            return;
        }
        $relatedData = $this->findRelatedData($surveyId, $responseId);
        if (empty($relatedData)) {
            return;
        }
        $this->fillExtraQuestions($surveyId, $responseId);
        /* Always unset current srid */
        Yii::app()->session['matchDataControl'] = null;
    }

    /**
     * Find and return the related data, save it in session (during survey)
     * @param integer $surveyId
     * @param integer $srid
     * @param boolean $force
     * @return null|false|array
     */
    private function findRelatedDataSession($surveyId, $srid)
    {
        $matchDataControlSession = Yii::app()->session['matchDataControl'];
        if (isset($matchDataControlSession[$surveyId][$srid])) {
            return $matchDataControlSession[$surveyId][$srid];
        }
        $findRelatedData = $this->findRelatedData($surveyId, $srid, true);
        $matchDataControlSession[$surveyId][$srid] = $findRelatedData;
        Yii::app()->session['matchDataControl'] = $matchDataControlSession;
        return $findRelatedData;
    }
    /**
     * Find and return the related data
     * @param integer $surveyId
     * @param integer $srid
     * @param boolean $force
     * @return null|false|array
     */
    private function findRelatedData($surveyId, $srid, $session = false)
    {
        /* Check $oResponse existence */
        $oResponse = Response::model($surveyId)->find("id = :id", array(":id" => $srid));
        if (empty($oResponse)) {
            $this->log("Invalid response id $srid for survey $surveyId", 'warning');
            return false;
        }
        $language = Survey::model()->findByPk($surveyId)->language;
        $matchQuestion1 = $this->get('matchQuestion1', 'Survey', $surveyId, null);
        $matchQuestion2 = $this->get('matchQuestion2', 'Survey', $surveyId, null);
        $otherFilters = $this->getOtherFilters($surveyId);
        $matchQuestionData = $this->get('matchQuestionData', 'Survey', $surveyId, null);
        $matchQuestionCount = $this->get('matchQuestionCount', 'Survey', $surveyId, null);
        $matchQuestionState = $this->get('matchQuestionState', 'Survey', $surveyId, null);
        $matchedQuestions = $this->get('matchedQuestions', 'Survey', $surveyId, null);
        if (empty($matchQuestion1) || empty($matchQuestion2)) {
            return false;
        }
        if (empty($matchedQuestions)) {
            return false;
        }

        /* Find current values of matchQuestion 1 & 2 */
        $oQuestionCriteria = new CDbCriteria();
        $oQuestionCriteria->condition = "t.sid =:sid and parent_qid = 0";
        $oQuestionCriteria->params = array(":sid" => $surveyId);
        $oQuestionCriteria->addInCondition("type", ['L','O','!','*','S','T','U']);
        $oQuestionCriteria->compare("title", $matchQuestion1);
        $oQuestion = Question::model()->find($oQuestionCriteria);
        if (empty($oQuestion)) {
            $this->log("Error in matchQuestion1 : $matchQuestion1 not found in survey $surveyId", 'error');
            return false;
        }
        $matchColumn1 = $surveyId . "X" . $oQuestion->gid . "X" . $oQuestion->qid;

        $oQuestionCriteria = new CDbCriteria();
        $oQuestionCriteria->condition = "t.sid =:sid and parent_qid = 0";
        $oQuestionCriteria->params = array(":sid" => $surveyId);
        $oQuestionCriteria->addInCondition("type", ['L','O','!','*','S','T','U']);
        $oQuestionCriteria->compare("title", $matchQuestion2);
        $oQuestion = Question::model()->find($oQuestionCriteria);
        if (empty($oQuestion)) {
            $this->log("Error in matchQuestion2 : $matchQuestion2 not found in survey $surveyId", 'error');
            return false;
        }
        $matchColumn2 = $surveyId . "X" . $oQuestion->gid . "X" . $oQuestion->qid;

        $result = array(
            'state' => null,
            'srid' => null,
            'matched' => array()
        );

        if (empty($oResponse->getAttribute($matchColumn2)) || empty($oResponse->getAttribute($matchColumn2))) {
            /* Return but not fill SESSION */
            $this->emptyExtraQuestion($surveyId, $srid);
            return null;
        }
        $oResponseCriteria = new CDbCriteria();
        $oResponseCriteria->compare(Yii::app()->db->quoteColumnName($matchColumn1), $oResponse->getAttribute($matchColumn2));
        $oResponseCriteria->compare(Yii::app()->db->quoteColumnName($matchColumn2), $oResponse->getAttribute($matchColumn1));
        $otherFilters = $this->get('otherFilters', 'Survey', $surveyId, null);
        if (!empty($otherFilters)) {
            $aCodeQuestions = array_flip(getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId));
        } else {
            $otherFilters = array();
            $aCodeQuestions = array();
        }
        foreach ($otherFilters as $questionFilter) {
            $sColumn = null;
            if (isset($aCodeQuestions[$questionFilter])) {
                $sColumn = $aCodeQuestions[$questionFilter];
            }
            if ($sColumn && !empty($oResponse->getAttribute($sColumn))) {
                $oResponseCriteria->compare(Yii::app()->db->quoteColumnName($sColumn), $oResponse->getAttribute($sColumn));
            }
        }
        $oResponseRelated = Response::model($surveyId)->find($oResponseCriteria);
        if (empty($oResponseRelated)) {
            $this->emptyExtraQuestion($surveyId, $srid);
            return null;
        }
        $result['state'] = empty($oResponseRelated->submitdate) ? 'draft' : 'completed';
        $result['srid'] = $oResponseRelated->id;

        foreach ($matchedQuestions as $matchedQuestion) {
            $oQuestionCriteria = new CDbCriteria();
            $oQuestionCriteria->condition = "t.sid =:sid and parent_qid = 0";
            $oQuestionCriteria->params = array(":sid" => $surveyId);
            $oQuestionCriteria->addInCondition("type", ['L','O','!','N','*','S','T','U']);
            $oQuestionCriteria->compare("title", $matchedQuestion);
            $oQuestion = Question::model()->find($oQuestionCriteria);
            if (empty($oQuestion)) {
                $this->log("Error in matchedQuestions : $matchedQuestion not found or invalid in survey $surveyId", 'error');
                continue;
            }
            $response = $oResponseRelated->getAttribute($oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid);
            $type = 'string';
            if (in_array($oQuestion->type, array('N','K'))) {
                $type = 'number';
            }
            $result['matched'][$matchedQuestion] = array(
                'code' => $matchedQuestion,
                'value' => $this->fixDbValue($response, $type),
                'type' => $type,
            );
        }
        return $result;
    }

    public function beforeQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('surveyId');
        if (empty($this->get('matchQuestionData', 'Survey', $surveyId, null))) {
            $this->unsubscribe('beforeQuestionRender');
        }
        $type = $this->getEvent()->get('type');
        $code = $this->getEvent()->get('code');
        if ($code == $this->get('matchQuestionData', 'Survey', $surveyId, null) && in_array($type, ['T','U'])) {
            $questionRenderEvent = $this->getEvent();
            $answer = $this->getQuestionAnswerHtml($surveyId, $questionRenderEvent->get('qid'));
            $questionRenderEvent->set("answers", $answer);
        }
    }

    public function newDirectRequest()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->event->get('target') != get_class()) {
            return;
        }
        $surveyid = Yii::app()->getRequest()->getParam('surveyid', Yii::app()->getRequest()->getParam('sid'));
        if (empty($surveyid)) {
            throw new CHttpException(400, $this->translate("No surveyid"));
        }
        if (empty(Survey::model()->findByPk($surveyid))) {
            throw new CHttpException(404, $this->translate("Invalid survey id"));
        }
        $function = $this->event->get('function');
        $token = Yii::app()->getRequest()->getParam('token');
        $srid = Yii::app()->getRequest()->getParam('srid');
        $language = Yii::app()->getRequest()->getParam('lang');
        switch ($function) {
            case 'export':
            default:
                $this->exportDifferences($surveyid, $token, $srid, $language);
        }
    }

    private function getQuestionAnswerHtml($surveyId, $qid)
    {
        $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
        $sgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        $aSurveyInfo = getSurveyInfo($surveyId, Yii::app()->getLanguage());
        $this->subscribe('getPluginTwigPath');
        if (empty($_SESSION['survey_' . $surveyId]['srid'])) {
            return Yii::app()->twigRenderer->renderPartial('/survey/questions/answer/longfreetext/matchdata-noanswer.twig', array(
                'message' => $this->gT("No compare data when survey not active."),
                'name' => $sgqa,
                'aSurveyInfo' => $aSurveyInfo,
            ));
        }
        $srid = $_SESSION['survey_' . $surveyId]['srid'];
        $matchDataControlSession = Yii::app()->session['matchDataControl'];
        if (empty($matchDataControlSession[$surveyId][$srid])) {
            return Yii::app()->twigRenderer->renderPartial(
                '/survey/questions/answer/longfreetext/matchdata-noanswer.twig',
                array(
                    'message' => $this->translate("No match data."),
                    'name' => $sgqa,
                    'aSurveyInfo' => $aSurveyInfo,
                )
            );
        }
        $matchData = $matchDataControlSession[$surveyId][$srid];

        $matchQuestion1 = $this->get('matchQuestion1', 'Survey', $surveyId, null);
        $matchQuestion2 = $this->get('matchQuestion2', 'Survey', $surveyId, null);
        $aMatched = array();
        foreach ($matchDataControlSession[$surveyId][$srid]['matched'] as $matched) {
            $matched['current'] = LimeExpressionManager::ProcessStepString("{" . $matched['code'] . ".shown}", array(), 3, 1);
            $matched['current'] = $this->fixDbValue($matched['current'], $matched['type']);
            $aMatched[] = $matched;
        }
        $aData = array(
            'name' => $sgqa,
            'aSurveyInfo' => $aSurveyInfo,
            'matchQuestion1' => $matchQuestion1,
            'matchQuestion2' => $matchQuestion2,
            'state' => $matchDataControlSession[$surveyId][$srid]['state'],
            'aMatched' => $aMatched,
            'lang' => array(
                'Related data are completed' => $this->gT("Related data are completed"),
                'Related data are not completed' => $this->gT("Related data are not completed"),
            ),
        );
        return Yii::app()->twigRenderer->renderPartial(
            '/survey/questions/answer/longfreetext/matchdata-answer.twig',
            $aData
        );
    }

    /**
     * Fill the extra questions part in response, no control of existence
     * @param integer $surveyId
     * @param integer $srid
     * @return @void
     */
    private function fillExtraQuestions($surveyId, $srid)
    {
        $matchDataControlSession = Yii::app()->session['matchDataControl'];
        if (empty($matchDataControlSession[$surveyId][$srid])) {
            return;
        }
        $matchData = $matchDataControlSession[$surveyId][$srid];
        $updatedData = array();

        $matchQuestionData = $this->get('matchQuestionData', 'Survey', $surveyId, null);
        $matchQuestionDataSgqa = null;
        if (!empty($matchQuestionData)) {
            $oQuestion = Question::model()->find("sid = :sid and title = :title", array(":sid" => $surveyId,":title" => $matchQuestionData));
            if ($oQuestion) {
                $matchQuestionDataSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
                $updatedData[$matchQuestionDataSgqa] = json_encode($matchData);
            }
        }
        $matchQuestionCount = $this->get('matchQuestionCount', 'Survey', $surveyId, null);
        $matchQuestionCountSgqa = null;
        if (!empty($matchQuestionCount)) {
            $oQuestion = Question::model()->find("sid = :sid and title = :title", array(":sid" => $surveyId,":title" => $matchQuestionCount));
            if ($oQuestion) {
                $matchQuestionCountSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
                $currentValue = isset($_SESSION['survey_' . $surveyId][$matchQuestionCountSgqa]) ? $_SESSION['survey_' . $surveyId][$matchQuestionCountSgqa] : "";
                $count = 0;
                foreach ($matchData['matched'] as $question => $matchedQuestion) {
                    $currentValue = LimeExpressionManager::ProcessStepString("{" . $question . ".NAOK}", array(), 3, true); // Maybe remove LimeExpressionManager dependance
                    if ($matchedQuestion['type'] == 'number') {
                        $currentValue = $this->fixDbValue($currentValue, 'number');
                        if (is_null($matchedQuestion['value'])) {
                            $matchedQuestion['value'] = '';
                        }
                        if (is_null($currentValue)) {
                            $currentValue = '';
                        }
                    }
                    if ($currentValue != $matchedQuestion['value']) {
                        $count++;
                    }
                }
                $updatedData[$matchQuestionCountSgqa] = $count;
            }
        }

        $matchQuestionState = $this->get('matchQuestionState', 'Survey', $surveyId, null);
        $matchQuestionStateSgqa = null;
        if (!empty($matchQuestionState)) {
            $oQuestion = Question::model()->find("sid = :sid and title = :title", array(":sid" => $surveyId,":title" => $matchQuestionState));
            if ($oQuestion) {
                $matchQuestionStateSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
                $updatedData[$matchQuestionStateSgqa] = $matchData['state'];
            }
        }

        if (empty($updatedData)) {
            return;
        }
        Response::model($surveyId)->updateByPk($srid, $updatedData);
        if ($_SESSION['survey_' . $surveyId]['srid'] == $srid) {
            foreach ($updatedData as $column => $value) {
                $_SESSION['survey_' . $surveyId][$column] = $value;
            }
        }
        /* Related one */
        if (!Response::model($surveyId)->findByPk($matchData['srid'])) {
            traceca($matchData['srid'] . " not found");
            return;
        }
        $relatedData = $this->findRelatedData($surveyId, $matchData['srid']);
        $relatedUpdateData = array();
        if ($matchQuestionDataSgqa) {
            $relatedUpdateData[$matchQuestionDataSgqa] = json_encode($relatedData);
        }
        if ($matchQuestionCountSgqa) {
            $relatedUpdateData[$matchQuestionCountSgqa] = $count;
        }
        if ($matchQuestionStateSgqa) {
            $state = 'draft';
            if (!empty(Response::model($surveyId)->findByPk($srid)->submitdate)) {
                $state = 'completed';
            }
            $relatedUpdateData[$matchQuestionStateSgqa] = $state;
        }
        if (!empty($relatedUpdateData)) {
            Response::model($surveyId)->updateByPk($matchData['srid'], $relatedUpdateData);
        }
    }

    /**
     * Put empty value in question DB and SESSION
     *
     * @return void
     */
    private function emptyExtraQuestion($surveyId, $srid)
    {
        $matchQuestionData = $this->get('matchQuestionData', 'Survey', $surveyId, null);
        $matchQuestionDataSgqa = null;
        if (!empty($matchQuestionData)) {
            $oQuestion = Question::model()->find("sid = :sid and title = :title", array(":sid" => $surveyId,":title" => $matchQuestionData));
            if ($oQuestion) {
                $matchQuestionDataSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
                $updatedData[$matchQuestionDataSgqa] = null;
            }
        }
        $matchQuestionCount = $this->get('matchQuestionCount', 'Survey', $surveyId, null);
        $matchQuestionCountSgqa = null;
        if (!empty($matchQuestionCount)) {
            $oQuestion = Question::model()->find("sid = :sid and title = :title", array(":sid" => $surveyId,":title" => $matchQuestionCount));
            if ($oQuestion) {
                $matchQuestionCountSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
                $updatedData[$matchQuestionCountSgqa] = null;
            }
        }

        $matchQuestionState = $this->get('matchQuestionState', 'Survey', $surveyId, null);
        $matchQuestionStateSgqa = null;
        if (!empty($matchQuestionState)) {
            $oQuestion = Question::model()->find("sid = :sid and title = :title", array(":sid" => $surveyId,":title" => $matchQuestionState));
            if ($oQuestion) {
                $matchQuestionStateSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
                $updatedData[$matchQuestionStateSgqa] = null;
            }
        }

        if (empty($updatedData)) {
            return;
        }
        Response::model($surveyId)->updateByPk($srid, $updatedData);
        if ($_SESSION['survey_' . $surveyId]['srid'] == $srid) {
            foreach ($updatedData as $column => $value) {
                $_SESSION['survey_' . $surveyId][$column] = $value;
            }
        }
    }

    /**
     * Export difference, use request parameters for options
     * @param int $surveyId
     * @param string $token
     * @param integer $srid
     * @param string $language
     * @return void
     */
    private function exportDifferences($surveyId, $token = null, $srid = null, $language = null)
    {
        Yii::import('application.helpers.viewHelper');
        /* Check if $srid is OK */
        if (!Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export') && $srid) {
            $sessionSrid = isset($_SESSION['survey_' . $surveyId]['srid']) ? $_SESSION['survey_' . $surveyId]['srid'] : null;
            if ($sessionSrid != $srid) {
                throw new CHttpException(403, $this->translate("No rights on this survey."));
            }
        }
        if (empty($srid) && empty($token) && empty(Yii::app()->session['loginID'])) {
            throw new CHttpException(401, $this->translate("No rights on this survey."));
        }
        if (empty($srid) && empty($token) && !Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export')) {
            throw new CHttpException(403, $this->translate("No rights on this survey."));
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!empty($token) && ($oSurvey->anonymized == 'Y' || !$oSurvey->hasTokensTable)) {
            throw new CHttpException(400, $this->translate("Invalid token parameter for this survey."));
        }
        if (empty($language) || !in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        $useExpression = false;
        $useExpressionSetting = $this->get('useexpression', 'Survey', $surveyId, 0);
        if ($useExpressionSetting == 'all') {
            $useExpression = true;
        }
        if ($useExpressionSetting == 'token' && $token) {
            $useExpression = true;
        }
        $aLanguageForData = array(
            'No match data' => $this->translate('No match data'),
        );
        $aNomatchdata = $this->get('nomatchstring', 'Survey', $surveyId);
        if (!empty($aNomatchdata[$language])) {
            $aLanguageForData['No match data'] = $aNomatchdata[$language];
        }
        if ($useExpression) {
            $aLanguageForData['No match data'] = $this->ProcessString($aLanguageForData['No match data']);
        }
        /* Fill needed data */
        $matchQuestion1 = $this->get('matchQuestion1', 'Survey', $surveyId, null);
        $matchQuestion2 = $this->get('matchQuestion2', 'Survey', $surveyId, null);
        $matchQuestionData = $this->get('matchQuestionData', 'Survey', $surveyId, null);
        $matchQuestionCount = $this->get('matchQuestionCount', 'Survey', $surveyId, null);
        $matchQuestionState = $this->get('matchQuestionState', 'Survey', $surveyId, null);
        $matchedQuestions = $this->get('matchedQuestions', 'Survey', $surveyId, null);
        if (empty($matchQuestion1) || empty($matchQuestion2) || empty($matchQuestionData) || empty($matchedQuestions)) {
            throw new CHttpException(400, $this->translate("Invalid survey for this action."));
        }
        $matchQuestionData = $this->get('matchQuestionData', 'Survey', $surveyId, null);
        $oQuestion = Question::model()->find("sid = :sid and title = :title", array(":sid" => $surveyId,":title" => $matchQuestionData));
        if (empty($oQuestion)) {
            if (Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
                throw new CHttpException(400, $this->translate("Invalid question $matchQuestionData for this survey."));
            }
            $this->log("Error in matchQuestionData : $matchQuestionData not found for survey $surveyId", 'error');
            throw new CHttpException(400, $this->translate("Invalid survey for this action."));
        }
        $matchColumn = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        $quotedMatchColumn = Yii::app()->getDb()->quoteColumnName($matchColumn);

        $criteria = new CDbCriteria();
        $header = array();
        $exportNoMatched = $this->get('exportNoMatched', 'survey', $surveyId, 1);
        $oQuestion = $this->getQuestionByTitle($surveyId, $matchQuestion1);
        if (in_array($oQuestion->type, array('L','O','!'))) {
            $AnswersCode = CHtml::listData(
                \Answer::model()->findAll(array(
                    'condition' => "qid = :qid",
                    'order' => 'sortorder ASC',
                    'params' => array(":qid" => $oQuestion->qid)
                )),
                'aid',
                'code'
            );
            $matchQuestion1Answers = array();
            foreach ($AnswersCode as $aid => $code) {
                $AnswerL10n = AnswerL10n::model()->find(
                    "aid=:aid AND language=:language",
                    array(":aid" => $aid, ":language" => $language)
                );
                if ($AnswerL10n) {
                    $matchQuestion1Answers[$code] = $AnswerL10n->answer;
                }
            }
        }
        $matchQuestion1Sgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        $oQuestionL10N = QuestionL10n::model()->find(
            'qid = :qid and language = :language',
            array(":qid" => $oQuestion->qid, ":language" => $language)
        );
        $questiontext = $oQuestionL10N->question;
        if ($useExpression) {
            $questiontext = $questiontext;
        }
        $header[] = viewHelper::flatEllipsizeText($questiontext);
        $oQuestion = $this->getQuestionByTitle($surveyId, $matchQuestion2);
        if (in_array($oQuestion->type, array('L','O','!'))) {
            $AnswersCode = CHtml::listData(
                \Answer::model()->findAll(array(
                    'condition' => "qid = :qid",
                    'order' => 'sortorder ASC',
                    'params' => array(":qid" => $oQuestion->qid)
                )),
                'aid',
                'code'
            );
            $matchQuestion2Answers = array();
            foreach ($AnswersCode as $aid => $code) {
                $AnswerL10n = AnswerL10n::model()->find(
                    "aid=:aid AND language=:language",
                    array(":aid" => $aid, ":language" => $language)
                );
                if ($AnswerL10n) {
                    $matchQuestion2Answers[$code] = $AnswerL10n->answer;
                }
            }
        }
        $matchQuestion2Sgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        $oQuestionL10N = QuestionL10n::model()->find(
            'qid = :qid and language = :language',
            array(":qid" => $oQuestion->qid, ":language" => $language)
        );
        $questiontext = $oQuestionL10N->question;
        $header[] = viewHelper::flatEllipsizeText($questiontext);
        $header[] = $this->translate("Question");
        $criteria->addCondition(Yii::app()->getDb()->quoteColumnName($matchQuestion1Sgqa) . " IS NOT NULL");
        $criteria->addCondition(Yii::app()->getDb()->quoteColumnName($matchQuestion1Sgqa) . " <> '' ");
        $criteria->addCondition(Yii::app()->getDb()->quoteColumnName($matchQuestion2Sgqa) . " IS NOT NULL");
        $criteria->addCondition(Yii::app()->getDb()->quoteColumnName($matchQuestion2Sgqa) . " <> ''");
        if (!$exportNoMatched) {
            $criteria->addCondition(Yii::app()->getDb()->quoteColumnName($matchColumn) . " IS NOT NULL");
            $criteria->addCondition(Yii::app()->getDb()->quoteColumnName($matchColumn) . " <> ''");
        }
        if ($token) {
            $criteria->compare('token', $token);
        }
        if ($srid) {
            $criteria->compare('id', $srid);
        }
        /* Other available columns */
        $otherFilters = $this->getOtherFilters($surveyId);
        $extraQuestions = array();
        if (App()->getRequest() && !empty($otherFilters)) {
            $aCodeQuestions = array_flip(getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId));
            foreach($otherFilters as $questionFilter) {
                $sColumn = null;
                if (isset($aCodeQuestions[$questionFilter])) {
                    $sColumn = $aCodeQuestions[$questionFilter];
                }
                if ($sColumn && App()->getRequest()->getParam($questionFilter)) {
                    $criteria->compare(App()->db->quoteColumnName($sColumn), App()->getRequest()->getParam($questionFilter));
                }
            }
        }
        /* Construct the array of question to get and compare */
        $aMatchedQuestionCode = array();
        $aMatchedQuestionText = array();
        foreach ($matchedQuestions as $matchedQuestion) {
            $oQuestion = $this->getQuestionByTitle($surveyId, $matchedQuestion);
            if (empty($oQuestion)) {
                $this->log("Error in matchedQuestions : $matchedQuestion not found or invalid in survey $surveyId", 'error');
                continue;
            }
            $oQuestionL10N = QuestionL10n::model()->find(
                'qid = :qid and language = :language',
                array(":qid" => $oQuestion->qid, ":language" => $language)
            );
            $aMatchedQuestionCode[$matchedQuestion] = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
            $questiontext = $oQuestionL10N->question;
            if ($useExpression) {
                $questiontext = $questiontext;
            }
            $aMatchedQuestionText[$matchedQuestion] = viewHelper::flatEllipsizeText($questiontext);
            if (empty($aMatchedQuestionText[$matchedQuestion])) {
                $aMatchedQuestionText[$matchedQuestion] = $oQuestionL10N->title;
            }
        }
        /* continue header */
        $matchQuestionStateSgqa = null;
        $exportState = $this->get('exportState', 'survey', $surveyId, 1);
        if ($matchQuestionState && $exportState) {
            $oQuestion = $this->getQuestionByTitle($surveyId, $matchQuestionState);
            if (!empty($oQuestion)) {
                $oQuestionL10N = QuestionL10n::model()->find(
                    'qid = :qid and language = :language',
                    array(":qid" => $oQuestion->qid, ":language" => $language)
                );
                $questiontext = $oQuestionL10N->question;
                if ($useExpression) {
                    $questiontext = $questiontext;
                }
                $header[] = viewHelper::flatEllipsizeText($questiontext);
                $matchQuestionStateSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
            }
        }
        $matchQuestionCountSgqa = null;
        $exportCount = $this->get('exportCount', 'survey', $surveyId, 1);
        if ($matchQuestionCount && $exportCount) {
            $oQuestion = $this->getQuestionByTitle($surveyId, $matchQuestionCount);
            if (!empty($oQuestion)) {
                $oQuestionL10N = QuestionL10n::model()->find(
                    'qid = :qid and language = :language',
                    array(":qid" => $oQuestion->qid, ":language" => $language)
                );
                $questiontext = $oQuestionL10N->question;
                if ($useExpression) {
                    $questiontext = $questiontext;
                }
                $header[] = viewHelper::flatEllipsizeText($questiontext);
                $matchQuestionCountSgqa = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
            }
        }
        $oQuestion = $this->getQuestionByTitle($surveyId, $matchQuestion1);
        if (!empty($oQuestion)) {
            $oQuestionL10N = QuestionL10n::model()->find(
                'qid = :qid and language = :language',
                array(":qid" => $oQuestion->qid, ":language" => $language)
            );
            $questiontext = $oQuestionL10N->question;
            if ($useExpression) {
                $questiontext = $questiontext;
            }
            $header[] = viewHelper::flatEllipsizeText(sprintf($this->translate("Value for %s"), $questiontext));
        }
        $oQuestion = $this->getQuestionByTitle($surveyId, $matchQuestion2);
        if (!empty($oQuestion)) {
            $oQuestionL10N = QuestionL10n::model()->find(
                'qid = :qid and language = :language',
                array(":qid" => $oQuestion->qid, ":language" => $language)
            );
            $questiontext = $oQuestionL10N->question;
            if ($useExpression) {
                $questiontext = $questiontext;
            }
            $header[] = viewHelper::flatEllipsizeText(sprintf($this->translate("Value for %s"), $questiontext));
        }

        /* Get lines by lines */
        $state = Yii::app()->getRequest()->getParam('state'); // completed
        $onlydiff = Yii::app()->getRequest()->getParam('diff'); // Only difference
        $mapdiff = Yii::app()->getRequest()->getParam('matchdiff'); // Only difference in matching
        $aData = array();

        $aoResponse = Response::model($surveyId)->findAll($criteria);
        //~ App()->end();
        foreach ($aoResponse as $oResponse) {
            $aResponseDatas = array();
            $match1Response = $oResponse->getAttribute($matchQuestion1Sgqa);
            if (isset($matchQuestion1Answers[$match1Response])) {
                $match1Response = viewHelper::flatEllipsizeText($matchQuestion1Answers[$match1Response]);
            }
            $match2Response = $oResponse->getAttribute($matchQuestion2Sgqa);
            if (isset($matchQuestion2Answers[$match2Response])) {
                $match2Response = viewHelper::flatEllipsizeText($matchQuestion2Answers[$match2Response]);
            }
            if (empty($oResponse->getAttribute($matchColumn))) {
                if ($exportNoMatched) {
                    $aResponseData = array(
                        $match1Response,
                        $match2Response,
                        '',
                    );
                    if ($matchQuestionStateSgqa) {
                        $aResponseData[] = '';
                    }
                    if ($matchQuestionCountSgqa) {
                        $aResponseData[] = '';
                    }
                    $aResponseData[] = '';
                    $aResponseData[] = $aLanguageForData['No match data'];
                    $aResponseDatas[] = $aResponseData;
                    $aData =  array_merge($aData, $aResponseDatas);
                }
                continue;
            }
            $dataMatched = json_decode($oResponse->getAttribute($matchColumn), 1);
            if (!empty($state) && (empty($dataMatched['state']) || $state != $dataMatched['state'])) {
                continue;
            }
            if (!$exportNoMatched && empty($dataMatched['matched'])) {
                continue;
            }

            $stateResponse = null;
            if ($matchQuestionStateSgqa) {
                $stateResponse = $oResponse->getAttribute($matchQuestionStateSgqa);
            }
            $countResponse = null;
            if ($matchQuestionCountSgqa) {
                $countResponse = $oResponse->getAttribute($matchQuestionCountSgqa);
            }

            $matchData = $dataMatched['matched'];
            $countDiff = 0;
            foreach ($aMatchedQuestionCode as $title => $column) {
                $responseValue = $oResponse->getAttribute($column);
                if (empty($matchData[$title])) {
                    continue;
                }
                $aResponseData = array(
                    $match1Response,
                    $match2Response,
                    $aMatchedQuestionText[$title]
                );
                if ($matchQuestionStateSgqa) {
                    $aResponseData[] = strval($stateResponse);
                }
                if ($matchQuestionCountSgqa) {
                    $aResponseData[] = strval($countResponse);
                }
                $responseValue = $this->fixDbValue($responseValue, $matchData[$title]['type']);
                $matchedValue = $matchData[$title]['value'];
                if ($responseValue != $matchedValue) {
                    $countDiff++;
                }
                if ($onlydiff && $responseValue == $matchedValue) {
                    continue;
                }
                $aResponseData[] = $responseValue;
                $aResponseData[] = $matchedValue;
                $aResponseDatas[] = $aResponseData;
            }
            if ($mapdiff && !$countDiff) {
                continue;
            }
            $aData = array_merge($aData, $aResponseDatas);
        }
        $aCurrentFilename = $this->get('filename', 'Survey', $surveyId);
        $filename = 'match_' . $surveyId;

        if (!empty($aCurrentFilename[$language])) {
            $filename = $aCurrentFilename[$language];
            if ($useExpression) {
                $filename = $this->ProcessString($filename);
            }
            $filename = sanitize_filename($filename, false, false, false);
        }
        $this->exportXls($header, $aData, $filename);
        Yii::app()->end();
    }

    /**
     * Get the fixed other filters
     * @param $surveyId
     * return string[]
     */
    private function getOtherFilters($surveyId)
    {
        $otherFilters = $this->get('otherFilters', 'Survey', $surveyId, null);
        if (empty($otherFilters) || !Yii::getPathOfAlias('getQuestionInformation')) {
            return array();
        }
        $otherFilters = array_flip($otherFilters);
        $matchQuestion1 = $this->get('matchQuestion1', 'Survey', $surveyId, null);
        if ($matchQuestion1) {
            unset($otherFilters[$matchQuestion1]);
        }
        $matchQuestion2 = $this->get('matchQuestion2', 'Survey', $surveyId, null);
        if ($matchQuestion2) {
            unset($otherFilters[$matchQuestion2]);
        }
        return array_flip($otherFilters);
    }

    /**
     * Get question object
     * @param $string title
     * @param $string[] types
     * return null|\Question
     */
    private function getQuestionByTitle($surveyId, $title, $types = array('L','O','!','N','*','S','T','U'))
    {
        $oQuestionCriteria = new CDbCriteria();
        $oQuestionCriteria->condition = "t.sid =:sid and parent_qid = 0";
        $oQuestionCriteria->params = array(":sid" => $surveyId);
        $oQuestionCriteria->addInCondition("type", $types);
        $oQuestionCriteria->compare("title", $title);
        return Question::model()->find($oQuestionCriteria);
    }

    /**
     * Export (and quit) data as excel
     * @param string[] the header
     * $param string[][] the data
     * @return void
     */
    private function exportXls($header, $aData, $name)
    {
        viewHelper::disableHtmlLogging();
        $workbook = new XLSXWriter();
        $workbook->setTempDir(Yii::app()->getConfig('tempdir'));
        $workbook->writeSheetRow($name, $header);
        foreach ($aData as $data) {
            $workbook->writeSheetRow($name, $data);
        }
        $filename = Yii::app()->getConfig("tempdir") . DIRECTORY_SEPARATOR . Yii::app()->securityManager->generateRandomString(42);
        $workbook->writeToFile($filename);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=\"{$name}.xlsx\"");
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        unlink($filename);
    }

    /**
     * Fix the DB value
     * $value mixed
     * $type string
     * @return mixed
     */
    private function fixDbValue($value, $type)
    {
        if ($type == 'number') {
            if ($value && $value[0] == ".") {
                $value = "0" . $value;
            }
            if (strpos($value, ".")) {
                $value = rtrim(rtrim($value, "0"), ".");
            }
        }
        return $value;
    }

    /**
     * Process a string by EM
     * @param string $string
     * @return string
     */
    private function processString($string)
    {
        /* Commented : work for date even if not initilized */
        //~ if(!LimeExpressionManager::isInitialized()) {
            //~ return $string;
        //~ }
        return LimeExpressionManager::ProcessStepString($string, array(), 3, true);
    }
}
