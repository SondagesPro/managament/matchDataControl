# matchDataControl

Match data by selecting 2 related question in a survey.

Possible usage are related to token and partnership.


## Settings

Settings are accessible by the tool menu, you can set

- **Questions for matching**
    - _Question for matching_ : the current response for matching, example : a country code prefilled by token value.
    - _Related question for matching_ : the related question for matching : for example the partner ISO counry code
- **Questions for data and resume**
    - _Question for matching data_ : where matching data are saved.
    This question can be used to show a table to participant with difference.
    This table use twig file in `survey/questions/answer/longfreetext` directory, then can be replaced by your own HTML.
    - _Question for count of difference_ : count the number of different response in matched data
    - _Question for state_ : the state of matched reponse :
        - empty : no related response
        - `draft` : not submitted response
        - `completed` : submitted response
- **Questions to be matched**
    - _Question to be matched_ : The list of questions to be matched. Plugin use question code to be exported or copied.
     Matched question can be only single question, no array or multiple question.
- **Export links**
    - _Export data without match_ : export a line when there are no related data to compare.
    In this case, no value is set for data, the data are set to _No match data._ for the line with the related value
    - _Links for download differences file_ : the direct links for downloading the data with match control
    - _URL to download differences file inside survey_ The constructed URL using current survey and token from survey.
    This link can only be used with token, with current response or for admin user.
    The parameters for url are
        - `token` : the token to be used : allow to download all match data related to this token.
        - `state` : the state of related data : `completed` or `draft`
        - `matchdiff` : match data exported have at minimum one difference
        - `diff` : export only difference

## Home page & Copyright
- HomePage <https://extensions.sondages.pro/>
- Copyright © 2020 Denis Chenu <https://sondages.pro>
- Copyright © 2020 OECD (Organisation for Economic Co-operation and Development) <https://www.oecd.org>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)
